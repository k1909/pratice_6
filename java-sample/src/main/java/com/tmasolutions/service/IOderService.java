package com.tmasolutions.service;

import com.tmasolutions.model.Oder;

public interface IOderService {

	Oder updateQuantityRequired(Long id, Long quantity);

	Oder updateCustomerIdRequired(Long id, Long Customer_id);

	Oder updatesetTotalPriceRequired(Long id, Long total_price);

}
