package com.tmasolutions.service;

import com.tmasolutions.model.Vendor;

public interface IVendorService {

	Vendor updateNameRequired(Long id, String name);

	Vendor updateCreditRequired(Long id, Long credit);

	Vendor updateEnableRequired(Long id, Boolean Boolean);



}
