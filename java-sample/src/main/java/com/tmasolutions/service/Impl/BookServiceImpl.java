package com.tmasolutions.service.Impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.PathVariable;

//import com.tmasolutions.model.Book;
//import com.tmasolutions.repo.BookRepository;
import com.tmasolutions.service.IBookService;

@Service

public class BookServiceImpl implements IBookService {

// 	@Autowired
//    private BookRepository bookRepository;
// 	
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED) 
//	public Book updateNameRequired(Long id, String name) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setName(name);
//		bookRepository.save(bk);
//        return bk;
//		
//	}
//	
//	@Transactional(propagation = Propagation.REQUIRED) 
//	@Override
//	public Book updateDescriptionRequired(Long id, String description) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setDescription(description);
//		bookRepository.save(bk);
//		return null;
//	}
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED) 
//	public Book updateLanguageRequired(Long id, String description) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setLanguage(description);	
//		bookRepository.save(bk);
//        int i = 1/0;
//        return bk;
//		
//	}
//	
//	@Override
//    @Transactional(propagation = Propagation.REQUIRED) 
//    public Book updateAuthorNameRequired(Long id, String authorName) {
//
//        Book bk = bookRepository.findById(id).get();
//        bk.setAuthor(authorName);
//        bookRepository.save(bk);
//        return bk;
//    }
//	
//	
//	
//	@Override
//	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
//	public Book updateNameReadOnly(Long id, String name) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setName(name);
//		bookRepository.save(bk);
//        return bk;
//		
//	}
//	
//	@Override
//	//@Transactional(rollbackFor = { SQLException.class })
//	//@Transactional(noRollbackFor = { SQLException.class})
//	//@Transactional(noRollbackFor = { SQLException.class})
//	@Transactional
//	public Book updateNameRollBack(Long id, String name) throws SQLException{
//		try {
//			Book bk = bookRepository.findById(id).get();
//			bk.setLanguage(name);	
//			bookRepository.save(bk);
//			int i = 1/0;
//			return bk;
//		}catch(Exception ex)
//		{
//			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//			throw new SQLException("Throwing exception for demoing rollback");
//		}
//	}
//	
//	
//	
//	
//
//	@Override
//	public List<Book> findByNameContaining(String name) {
//		return bookRepository.findByNameContaining(name);
//	}
//
//	@Override
//	@Transactional(propagation = Propagation.MANDATORY) 
//	public Book updateNameMandatory(Long id, String name) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setName(name);
//		bookRepository.save(bk);
//        return bk;
//		
//	}
//	
//	@Transactional(propagation = Propagation.MANDATORY) 
//	@Override
//	public Book updateDescriptionMandatory(Long id, String description) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setDescription(description);
//		bookRepository.save(bk);
//		return null;
//	}
//
//	@Override
//	@Transactional(propagation = Propagation.MANDATORY) 
//	public Book updateLanguageMandatory(Long id, String language) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setLanguage(language);	
//		bookRepository.save(bk);
//        int i = 1/0;
//        return bk;
//		
//	}
//	
//	@Override
//    @Transactional(propagation = Propagation.MANDATORY) 
//    public Book updateAuthorNameMandatory(Long id, String authorName) {
//
//        Book bk = bookRepository.findById(id).get();
//        bk.setAuthor(authorName);
//        bookRepository.save(bk);
//        return bk;
//    }
//	
//	
//	//Never
//	@Override
//	@Transactional(propagation = Propagation.NEVER) 
//	public Book updateNameNever(Long id, String name) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setName(name);
//		bookRepository.save(bk);
//        return bk;
//		
//	}
//	
//	
//	@Override
//	@Transactional(propagation = Propagation.NEVER) 
//	public Book updateDescriptionNever(Long id, String description) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setDescription(description);
//		bookRepository.save(bk);
//		return null;
//	}
//
//	@Override
//	@Transactional(propagation = Propagation.NEVER) 
//	public Book updateLanguageNever(Long id, String language) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setLanguage(language);	
//		bookRepository.save(bk);
//        int i = 1/0;
//        return bk;
//		
//	}
//	
//	@Override
//    @Transactional(propagation = Propagation.NEVER) 
//    public Book updateAuthorNameNever(Long id, String authorName) {
//
//        Book bk = bookRepository.findById(id).get();
//        bk.setAuthor(authorName);
//        bookRepository.save(bk);
//        return bk;
//    }
//	
//	
//
//	
//
//	@Override
//	@Transactional(propagation = Propagation.SUPPORTS) 
//	public Book updateNameSupports(Long id, String name) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setName(name);
//		bookRepository.save(bk);
//        return bk;
//		
//	}
//	
//	
//	@Override
//	@Transactional(propagation = Propagation.SUPPORTS) 
//	public Book updateDescriptionSupports(Long id, String description) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setDescription(description);
//		bookRepository.save(bk);
//		return null;
//	}
//
//	@Override
//	@Transactional(propagation = Propagation.SUPPORTS) 
//	public Book updateLanguageSupports(Long id, String language) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setLanguage(language);	
//		bookRepository.save(bk);
//        int i = 1/0;
//        return bk;
//		
//	}
//	
//	@Override
//    @Transactional(propagation = Propagation.SUPPORTS) 
//    public Book updateAuthorNameSupports(Long id, String authorName) {
//
//        Book bk = bookRepository.findById(id).get();
//        bk.setAuthor(authorName);
//        bookRepository.save(bk);
//        return bk;
//    }
//	
//	
//	@Override
//	@Transactional(propagation = Propagation.NOT_SUPPORTED) 
//	public Book updateNameNotSupports(Long id, String name) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setName(name);
//		bookRepository.save(bk);
//        return bk;
//		
//	}
//	
//	@Override
//	@Transactional(propagation = Propagation.NOT_SUPPORTED) 
//	public Book updateDescriptionNotSupports(Long id, String description) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setDescription(description);
//		bookRepository.save(bk);
//		return null;
//	}
//
//	@Override
//	@Transactional(propagation = Propagation.NOT_SUPPORTED) 
//	public Book updateLanguageNotSupports(Long id, String language) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setLanguage(language);	
//		bookRepository.save(bk);
//        int i = 1/0;
//        return bk;
//		
//	}
//	
//	@Override
//    @Transactional(propagation = Propagation.NOT_SUPPORTED) 
//    public Book updateAuthorNameNotSupports(Long id, String authorName) {
//
//        Book bk = bookRepository.findById(id).get();
//        bk.setAuthor(authorName);
//        bookRepository.save(bk);
//        return bk;
//    }
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRES_NEW) 
//	public Book updateNameRequiresNew(Long id, String name) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setName(name);
//		bookRepository.save(bk);
//        return bk;
//		
//	}
//	
//	@Override
//	@Transactional(propagation = Propagation.REQUIRES_NEW) 
//	public Book updateDescriptionRequiresNew(Long id, String description) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setDescription(description);
//		bookRepository.save(bk);
//		return null;
//	}
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRES_NEW) 
//	public Book updateLanguageRequiresNew(Long id, String language) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setLanguage(language);	
//		bookRepository.save(bk);
//        int i = 1/0;
//        return bk;
//		
//	}
//	
//	@Override
//    @Transactional(propagation = Propagation.REQUIRES_NEW) 
//    public Book updateAuthorNameRequiresNew(Long id, String authorName) {
//
//        Book bk = bookRepository.findById(id).get();
//        bk.setAuthor(authorName);
//        bookRepository.save(bk);
//        return bk;
//    }
//	
//	
//	
//	@Override
//	@Transactional(propagation = Propagation.NESTED) 
//	public Book updateNameNested(Long id, String name) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setName(name);
//		bookRepository.save(bk);
//        return bk;
//		
//	}
//	
//	@Override
//	@Transactional(propagation = Propagation.NESTED) 
//	public Book updateDescriptionNested(Long id, String description) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setDescription(description);
//		bookRepository.save(bk);
//		return null;
//	}
//
//	@Override
//	@Transactional(propagation = Propagation.NESTED) 
//	public Book updateLanguageNested(Long id, String language) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setLanguage(language);	
//		bookRepository.save(bk);
//        int i = 1/0;
//        return bk;
//		
//	}
//	
//	@Override
//    @Transactional(propagation = Propagation.NESTED) 
//    public Book updateAuthorNameNested(Long id, String authorName) {
//
//        Book bk = bookRepository.findById(id).get();
//        bk.setAuthor(authorName);
//        bookRepository.save(bk);
//        return bk;
//    }
	

}
