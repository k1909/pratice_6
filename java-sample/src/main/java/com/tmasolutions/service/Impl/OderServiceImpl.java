package com.tmasolutions.service.Impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.PathVariable;

import com.tmasolutions.model.Oder;
import com.tmasolutions.repo.OderRepository;
import com.tmasolutions.service.IOderService;

@Service

public class OderServiceImpl implements IOderService {

 	@Autowired
    private OderRepository oderRepository;

	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Oder updateQuantityRequired(Long id, Long quantity) {
		Oder bk = oderRepository.findById(id).get();
		bk.setQuantity(quantity);
		oderRepository.save(bk);
        return bk;
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED) 
	@Override
	public Oder updatesetTotalPriceRequired(Long id, Long total_price) {
		Oder bk = oderRepository.findById(id).get();
		bk.setTotal_price(total_price);
		oderRepository.save(bk);
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Oder updateCustomerIdRequired(Long id, Long Customer_id) {
		Oder bk = oderRepository.findById(id).get();
		bk.setCustomer_id(Customer_id);	
		oderRepository.save(bk);
        return bk;
		
	}
	
	

}