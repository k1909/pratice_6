package com.tmasolutions.service.Impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.PathVariable;

import com.tmasolutions.model.Product;
import com.tmasolutions.repo.ProductRepository;
import com.tmasolutions.service.IProductService;

@Service

public class ProductServiceImpl implements IProductService {

 	@Autowired
    private ProductRepository productRepository;

	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Product updateNameRequired(Long id, String name) {
		Product bk = productRepository.findById(id).get();
		bk.setName(name);
		productRepository.save(bk);
        return bk;
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED) 
	@Override
	public Product updatePriceRequired(Long id, Long price) {
		Product bk = productRepository.findById(id).get();
		bk.setPrice(price);
		productRepository.save(bk);
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Product updateVendorIdRequired(Long id, Long vendor_id) {
		Product bk = productRepository.findById(id).get();
		bk.setVendor_id(vendor_id);	
		productRepository.save(bk);
        return bk;
		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Product updateCurrentQuantityRequired(Long id, int current_quantity) {
		Product bk = productRepository.findById(id).get();
		bk.setCurrent_quantity(current_quantity);	
		productRepository.save(bk);
        return bk;
		
	}
		

}