package com.tmasolutions.service.Impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.PathVariable;

import com.tmasolutions.model.Oder;
import com.tmasolutions.model.Vendor;
import com.tmasolutions.repo.VendorRepository;
import com.tmasolutions.service.IVendorService;

@Service

public class VendorServiceImpl implements IVendorService {

 	@Autowired
    private VendorRepository vendorRepository;

	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Vendor updateNameRequired(Long id, String name) {
		Vendor bk = vendorRepository.findById(id).get();
		bk.setName(name);
		vendorRepository.save(bk);
        return bk;
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED) 
	@Override
	public Vendor updateCreditRequired(Long id, Long credit) {
		Vendor bk = vendorRepository.findById(id).get();
		bk.setCredit(credit);
		vendorRepository.save(bk);
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Vendor updateEnableRequired(Long id, Boolean Boolean) {
		Vendor bk = vendorRepository.findById(id).get();
		bk.setEnable(Boolean);	
		vendorRepository.save(bk);
        return bk;
		
	}
	

		

}