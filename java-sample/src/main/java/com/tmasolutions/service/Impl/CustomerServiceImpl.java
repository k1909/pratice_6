package com.tmasolutions.service.Impl;


import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.PathVariable;

import com.tmasolutions.model.Customer;
import com.tmasolutions.repo.CustomerRepository;
import com.tmasolutions.service.ICustomerService;

@Service

public class CustomerServiceImpl implements ICustomerService {

 	@Autowired
    private CustomerRepository customerRepository;

	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Customer updateNameRequired(Long id, String name) {
		Customer bk = customerRepository.findById(id).get();
		bk.setName(name);
		customerRepository.save(bk);
        return bk;
		
	}
	
	
//	@Transactional(propagation = Propagation.REQUIRED) 
//	@Override
//	public Customer getcreditRequired(Long id, String Nam) {
//		Book bk = bookRepository.findById(id).get();
//		bk.setDescription(description);
//		bookRepository.save(bk);
//		return null;
//	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Customer updateLanguageRequired(Long id, Long credit) {
		Customer bk = customerRepository.findById(id).get();
		bk.setCredit(credit);	
		customerRepository.save(bk);
        return bk;
		
	}

	//@Transactional(rollbackFor = { SQLException.class })
	//@Transactional(noRollbackFor = { SQLException.class})
	//@Transactional(noRollbackFor = { SQLException.class})\
	@Override
	@Transactional
	public Customer updateCreditRollBack(Long id, Long credit) throws SQLException{
		try {
			Customer bk = customerRepository.findById(id).get();
			bk.setCredit(credit);	
			customerRepository.save(bk);
			return bk;
		}catch(Exception ex)
		{
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			throw new SQLException("Throwing exception for demoing rollback");
		}
	}

	@Override
	@Transactional
	public List<Customer> findByNameContaining(String name) {
		return customerRepository.findByNameContaining(name);
	}


}
