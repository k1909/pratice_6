package com.tmasolutions.service;

import com.tmasolutions.model.Product;

public interface IProductService {


	Product updateNameRequired(Long id, String name);

	Product updateCurrentQuantityRequired(Long id, int current_quantity);

	Product updateVendorIdRequired(Long id, Long vendor_id);

	Product updatePriceRequired(Long id, Long price);

}
