package com.tmasolutions.service;

import java.sql.SQLException;
import java.util.List;

import com.tmasolutions.model.Customer;

public interface ICustomerService {

	Customer updateNameRequired(Long id, String name);

	Customer updateLanguageRequired(Long id, Long credit);

	Customer updateCreditRollBack(Long id, Long credit) throws SQLException;

	List<Customer> findByNameContaining(String name);

}
