package com.tmasolutions.model;

import javax.persistence.*;


/**
* The persistent class for the Oder database table.
* 
*/
@Entity
@Table(name = "oder")
public class Oder {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long product_id;

	private Long quantity;

	private Long total_price;

	private Long customer_id;

	public Oder() {
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Long getTotal_price() {
		return total_price;
	}

	public void setTotal_price(Long total_price) {
		this.total_price = total_price;
	}

	public Long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}
	
}