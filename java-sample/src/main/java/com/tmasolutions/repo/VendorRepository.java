package com.tmasolutions.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.tmasolutions.model.Vendor;

 
public interface VendorRepository extends JpaRepository<Vendor, Long> {
	List<Vendor> findByNameContaining(String name);
	
	List<Vendor> findByName(String name);
	//	List<Customer> findByLanguage(String language);
	
	//@Query(value="SELECT * FROM book where book.name like %?1%", nativeQuery = true)
	//List<Book> findByNameContaining(String name);
	
	//@Query(value="select b FROM Book b where b.name like %?1%")
}