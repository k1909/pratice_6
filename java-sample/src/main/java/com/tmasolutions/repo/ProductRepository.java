package com.tmasolutions.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tmasolutions.model.Product;


public interface ProductRepository extends JpaRepository<Product, Long> {
	List<Product> findByNameContaining(String name);
	
	List<Product> findByName(String name);
	//	List<Customer> findByLanguage(String language);
	
	//@Query(value="SELECT * FROM book where book.name like %?1%", nativeQuery = true)
	//List<Book> findByNameContaining(String name);
	
	//@Query(value="select b FROM Book b where b.name like %?1%")
}