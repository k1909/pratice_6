package com.tmasolutions.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.tmasolutions.model.Oder;

 
public interface OderRepository extends JpaRepository<Oder, Long> {
	List<Oder> findByNameContaining(String name);
	
	List<Oder> findByName(String name);
	//	List<Customer> findByLanguage(String language);
	
	//@Query(value="SELECT * FROM book where book.name like %?1%", nativeQuery = true)
	//List<Book> findByNameContaining(String name);
	
}