package com.tmasolutions.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.tmasolutions.model.Oder;
import com.tmasolutions.repo.OderRepository;

import com.tmasolutions.model.Vendor;
import com.tmasolutions.repo.VendorRepository;

import com.tmasolutions.model.Customer;
import com.tmasolutions.repo.CustomerRepository;

import com.tmasolutions.model.Product;
import com.tmasolutions.repo.ProductRepository;

import com.tmasolutions.service.Impl.BookServiceImpl;
import com.tmasolutions.service.Impl.CustomerServiceImpl;
import com.tmasolutions.service.Impl.OderServiceImpl;
import com.tmasolutions.service.Impl.ProductServiceImpl;
import com.tmasolutions.service.Impl.VendorServiceImpl;


import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {
	
	
	 @Autowired	 
	 OderRepository oderRepository;
	 
	 @Autowired
	 OderServiceImpl oderServiceImpl;

	 @Autowired	 
	 VendorRepository vendorRepository;
	 
	 @Autowired
	 VendorServiceImpl vendorServiceImpl;

	 @Autowired	 
	 CustomerRepository customerRepository;
	 
	 @Autowired
	 CustomerServiceImpl customerServiceImpl;

	 @Autowired	 
	 ProductRepository productRepository;
	 
	 @Autowired
	 ProductServiceImpl productServiceImpl;

	 
//    @GetMapping("/greeting")
//    public ResponseEntity<JsonNode> greeting() throws JsonProcessingException {
//        ObjectMapper mapper = new ObjectMapper();
//        //int i = 1/0;
//        JsonNode json = mapper.readTree("{\"Greeting\": \"Greetings from Spring Boot!\"}");
//        return ResponseEntity.ok(json);
//    }
//    
//    
//  
//    
    @Transactional
    @PostMapping("/customer")
    @ApiOperation(value = "Create new customer", notes = "Create new customer for system")
	public ResponseEntity<Customer> createUser(@RequestBody Customer customer) throws JsonProcessingException {
    	Customer dt = customerRepository.save(customer);
        System.out.println(dt.getName());
    	//int a = 1/0;
        return ResponseEntity.ok(dt);
    
    }
    
    @GetMapping("/customers")
    public List<Customer> all() {
      return customerRepository.findAll();
    }
    
////    @GetMapping("/books/filer")
////    public List<Book> all(String search) {
////    	
////      return bookServiceImpl.findByNameContaining(search);
////    }
//    
    @GetMapping("/customers/{id}")
    public Customer one(@PathVariable Long id) {      
      return customerRepository.findById(id).orElseThrow(() -> null);
    }
//    
//    @Transactional
//    @PutMapping("/books/{id}/required-no-store-data")
//    public Book updateOne(@PathVariable Long id, String NewName,  String Description, String NewAuthor, String NewLaguage) {      
//      bookServiceImpl.updateNameRequired(id, NewName); //isn't stored to DB
//      bookServiceImpl.updateAuthorNameRequired(id, NewAuthor); //isn't stored to DB
//      bookServiceImpl.updateDescriptionRequired(id, Description); //isn't stored to DB
//      return bookServiceImpl.updateLanguageRequired(id, NewLaguage); //isn't stored to DB
//    }
//    
//    
//    @Transactional
//    @PutMapping("/books/{id}/requires-new-storedata-except-language")
//    public Book updateOneNew(@PathVariable Long id, String NewName,  String Description, String NewAuthor, String NewLaguage) {      
//      bookServiceImpl.updateNameRequiresNew(id, NewName); //Is stored to DB
//      bookServiceImpl.updateAuthorNameRequiresNew(id, NewAuthor); //Is stored to DB
//      bookServiceImpl.updateDescriptionRequiresNew(id, Description); //Is stored to DB
//      return bookServiceImpl.updateLanguageRequiresNew(id, NewLaguage); //isn't stored to DB
//    }
//    
//    
//    @Transactional
//    @PutMapping("/books/{id}/a-test-case")
//    public Book updateOneNew1(@PathVariable Long id, String NewName,  String Description, String NewAuthor, String NewLaguage) {      
//      bookServiceImpl.updateNameMandatory(id, NewName); //Is stored to DB
//      bookServiceImpl.updateAuthorNameRequiresNew(id, NewAuthor); //Is stored to DB
//      bookServiceImpl.updateDescriptionRequiresNew(id, Description); //Is stored to DB
//      return bookServiceImpl.updateLanguageRequiresNew(id, NewLaguage); //isn't stored to DB
//    }
//    
//    
//    @Transactional
//    @PutMapping("/books/{id}/mandatory-passed-store-data")
//    public Book updateOneMandatory(@PathVariable Long id, String NewName) {      
//    	return bookServiceImpl.updateNameMandatory(id, NewName); //Passed at normal
//    }
//    
//    @PutMapping("/books/{id}/mandatory-failed-no-store-data")
//    public Book updateOneMandatoryFailed(@PathVariable Long id, String NewName) {      
//    	return bookServiceImpl.updateNameMandatory(id, NewName);  //Throw exception
//    }
//    
//
//    @PutMapping("/books/{id}/never-passed-store-data")
//    public Book updateOneNeverPassed(@PathVariable Long id, String NewName) {      
//    	return bookServiceImpl.updateNameNever(id, NewName); //Passed at normal
//    }
//    
//    @Transactional
//    @PutMapping("/books/{id}/never-failed-no-store-data")
//    public Book updateOneNeverFailed(@PathVariable Long id, String NewName) {      
//    	return bookServiceImpl.updateNameNever(id, NewName);  //Throw exception
//    }
//    
//    @Transactional
//    @PutMapping("/books/{id}/supports-hastransaction-will-be-rolledback")
//    public Book supportshastransactionwillberollback(@PathVariable Long id, String NewLanguage) {      
//    	return bookServiceImpl.updateLanguageSupports(id, NewLanguage);  //Throw exception
//    }
//    
//    @PutMapping("/books/{id}/supports-notransaction-will-storeData")
//    public Book supportsnotransactionwillbestoredData(@PathVariable Long id, String NewLanguage) {      
//    	return bookServiceImpl.updateLanguageSupports(id, NewLanguage);  //Throw exception
//    }
//    
//    
//    @Transactional
//    @PutMapping("/books/{id}/not-supports-hastransaction-will-storedData")
//    public Book notsupportshastransactionwillstoredData(@PathVariable Long id, String NewLanguage) {      
//    	return bookServiceImpl.updateLanguageNotSupports(id, NewLanguage);  //Throw exception
//    }
//    
//    @PutMapping("/books/{id}/not-supports-notransaction-will-storedData")
//    public Book notsupportsnotransactionwillbestoredData(@PathVariable Long id, String NewLanguage) {      
//    	return bookServiceImpl.updateLanguageNotSupports(id, NewLanguage);  //Throw exception
//    }
//    
//    
//    @Transactional
//    @PutMapping("/books/{id}/nested-storedata-except-language")
//    public Book updateOneNested(@PathVariable Long id, String NewName,  String Description, String NewAuthor, String NewLaguage) {      
//      bookServiceImpl.updateNameNested(id, NewName); //Is stored to DB
//      bookServiceImpl.updateAuthorNameNested(id, NewAuthor); //Is stored to DB
//      bookServiceImpl.updateDescriptionNested(id, Description); //Is stored to DB
//      return bookServiceImpl.updateLanguageNested(id, NewLaguage); //isn't stored to DB
//    }
//    
//    
//    @Transactional
//    @PutMapping("/books/{id}/name-transaction-readonly")
//    public Book updateOne(@PathVariable Long id, String NewName) {      
//    	return bookServiceImpl.updateNameReadOnly(id, NewName);
//    }
//    
//
//    @PutMapping("/books/{id}/rollbackname")
//    public Book rollbackname(@PathVariable Long id, String NewName) throws SQLException {      
//    	return bookServiceImpl.updateNameRollBack(id, NewName);
//    }
//
//    @PutMapping("/books/findByName")
//    public List<Book> findByName(@PathVariable String NewName) throws SQLException {      
//    	return userRepository.findByName(NewName);
//    }

}